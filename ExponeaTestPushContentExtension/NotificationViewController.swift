//
//  NotificationViewController.swift
//  ExponeaTestPushContentExtension
//
//  Created by Igor Liska on 13/05/2021.
//

import UIKit
import UserNotifications
import UserNotificationsUI
import ExponeaSDK_Notifications

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    let exponeaService = ExponeaNotificationContentService()
    
    func didReceive(_ notification: UNNotification) {
        exponeaService.didReceive(notification, context: extensionContext, viewController: self)
    }

}
