//
//  ContentView.swift
//  Exponea iOS Push Test
//
//  Created by Igor Liska on 13/05/2021.
//

import SwiftUI
import ExponeaSDK

struct ContentView: View {
    var body: some View {
        Text("Hello.").padding()
        Button("Configure Exponea SDK") {
            Exponea.logger.logLevel = .verbose
            Exponea.shared.configure(
                Exponea.ProjectSettings(
                    projectToken: "YOUR PROJECT TOKEN",
                    authorization: .token("YOUR AUTHORIZATION TOKEN"),
                    baseUrl: "https://api.exponea.com"
                ),
                pushNotificationTracking: .enabled(appGroup: "group.com.exponea.ExponeaSDK-Example2")
            )
            Exponea.shared.identifyCustomer(
              customerIds: ["registered" : "tester@a.c"],
              properties: ["email" : "tester@a.c"],
              timestamp: nil
            )
            UNUserNotificationCenter.current()
                .requestAuthorization(options: [.badge, .alert, .sound]) { (granted, _) in
                    if granted {
                        DispatchQueue.main.async {
                            UIApplication.shared.registerForRemoteNotifications()
                        }
                    }
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
